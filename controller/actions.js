module.exports.init  = function(server,playbackSocket,playlistAPI,assetServer) {

  server.on("slide:action",(a)=>{
    console.log("action:",a)
    playbackSocket.emit('slide:'+a.body)
  })

  server.on("asset:upload",(list)=>{
    list.body.forEach(assetServer.upload);
  })

  playbackSocket.on('state:current',(currentState)=>{
    console.log('recieved updated state:',currentState)
    server.send('slide:change',currentState);
  })

  playbackSocket.on('event:connection',(c)=>{
    console.log('recieved connection :',c)
    server.send('event:connection',c);
  })
  
  


  //request currentSlide Data
  playbackSocket.emit('state:get');

}