const fs = require('fs');
const request = require('request');
const url = 'http://localhost:8005/asset/';


module.exports.upload = (filePath)=>{
  return new Promise((resolve, reject)=>{
    const formData = {
      file:fs.createReadStream(filePath)
    }
    request.post({url,formData},(err,data)=>{
      if(!err)resolve(data);
      else reject(err);
    })
  })
}