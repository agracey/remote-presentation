import React, {Component} from 'react';
const remote = require('electron').remote;
const { dialog } = require('electron').remote;


export default class AssetUploadBtn extends Component {


  render() {
    return (
      <button className="upload-btn" onClick={this.openFileDiag.bind(this)}>
        {this.props.children}
      </button>
    );
  }

  openFileDiag(e){
    e.stopPropagation();
    e.preventDefault();

    dialog.showOpenDialog( (fileNames) => {
      if (fileNames === undefined) return;
      this.props.handleFileSelection(fileNames);
    }); 


  }
}