import React, {Component} from 'react';

//import SlideFactory from 'src/Presentational/SlideFactory.js'

const mapTextToLines = (l,i)=>{
  return (<div className="text-line" key={l+i}>{l}</div>)
}

/*
 * BUilding this as a Factory so that in the future, I can build a slide before it's displayed
 * This would allow me to add in a "slide:build_next" and "slide:transition" for giving transitions
 * 
 */
class PreviewScreen extends Component {


  render() {
    console.log(this.props.slideStatus)

    return (
      <div id="preview-screen">
        {this.renderLiveSlide()}
        {this.renderNextSlide()}
        
        <div id="controls">
          <button onClick={this.handleLastSlide.bind(this)}>Go Last</button>
          <button onClick={this.handleBlankSlide.bind(this)}>Blank</button>
          <button onClick={this.handleNextSlide.bind(this)}>Go Next</button>
        </div>
      </div>
    );
  }
  renderLiveSlide() {
    const s = this.props.slideStatus.currentSlide||{};

    const content = this.getContent(s.type,s.content)
    
    return (
    <div id="current-slide">
      <header>Live <span className="pull-right">{s.name}</span></header>
      
      <div className="content">
        {content}
      </div>
    </div>
    )
  }

  renderNextSlide() {
    const s = this.props.slideStatus.nextSlide||{};

    const content = this.getContent(s.type,s.content)

    return (
      <div id="next-slide">
        <header>Next <span className="pull-right">{s.name}</span></header>
        {content}
      </div>
    )
  }

  getContent(type,content) {
    switch (type) {
      case "text":
        return content.text.map(mapTextToLines);
      default:
        return null;
    }

  }

  handleNextSlide() {
    this.props.changeSlide('next');
  }

  handleLastSlide() {
    this.props.changeSlide('prev');
  }

  handleBlankSlide() {
    this.props.changeSlide('blank');
  }

}


export default PreviewScreen;