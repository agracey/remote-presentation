import React, {Component} from 'react';
import Client from 'electron-rpc/client';

import PreviewScreen from './Components/PreviewScreen.js'
import AssetUploadBtn from './Presentational/AssetUploadBtn.js'


export default class App extends Component {
  constructor(props) {
    super(props);
    this.client = new Client();

    this.state = {
      slideStatus:{
        currentSlide:{
          type:"blank"
        },
        nextSlide:{
          type:"blank"
        }
      }
    };
  }

  componentDidMount() {
    console.log('componentDidMount');
    //Add socket listeners here to change state
    this.client.on('slide:change', this.changeSlideStatus.bind(this));
  }

  changeSlideStatus(err,slideStatus) {
    console.log(slideStatus)
    this.setState({slideStatus});
  }

  render() {
    const Slide = this.state.current;

    return (
      <div>
        <PreviewScreen 
          slideStatus={this.state.slideStatus}
          changeSlide={this.handleChangeSlide.bind(this)}
        />
        <AssetUploadBtn handleFileSelection={this.handleFileSelection.bind(this)}>
          Upload Assets!
        </AssetUploadBtn>
      </div>
    );
  }



  handleFileSelection(files){
    this.client.request("asset:upload",files);
  }

  handleChangeSlide(action) {
    this.client.request("slide:action",action);
  }

}
