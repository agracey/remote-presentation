import React, {Component} from 'react';

import SlideFactory from './SlideFactory.js'

import Client from 'electron-rpc/client';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.client = new Client();

    this.state = {
      current:SlideFactory.build({
        background:{color:'black'},
        type:'text',
        content:{text:["Here's some text","And Another Line"]}
      }),
      slideState:'stopped'
    }
  }
  componentDidMount() {
    console.log('componentDidMount');
    this.client.request('system:init')
    this.client.on('slide:change', this.changeSlide.bind(this));
    this.client.on('slide:changeSlideState', this.changeSlideState.bind(this));
  }

  changeSlide (e,s){
    this.setState({current:SlideFactory.build(s), playState:"stopped"});
  }
  changeSlideState (e,s){
    this.setState({slideState:s});
  }

  render() {
    const Slide = this.state.current;

    return (
      <Slide />
    );
  }

}
