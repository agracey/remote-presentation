import React, {Component} from 'react';

/*
 * BUilding this as a Factory so that in the future, I can build a slide before it's displayed
 * This would allow me to add in a "slide:build_next" and "slide:transition" for giving transitions
 * 
 */
export default class SlideFactory {
  
  static build(slide) {

    if(slide.type=='text'){
      return buildTextSlide(slide);
    }
    if(slide.type=='image'){
      return buildImageSlide(slide);
    }
    if(slide.type=='video'){
      return buildVideoSlide(slide);
    }
  }

}


function buildTextSlide(slide){
  return class TextSlide extends Component {
    render(){
      console.log("slide:",slide)
      
      const text = slide.content.text.map((t,i)=>((<p key={'line'+i}>{t}</p>)));

      return (
        <div className="slide">
          <div className="text">{text}</div>
        </div>
      )
    }
  }
}

function buildImageSlide(slide){
  return class ImageSlide extends Component {
    render(){
      return (
        <div className="slide">
          <img className="image" src={slide.content.imgSrc}/>
        </div>
      )
    }
  }
}
function buildVideoSlide(slide){
  
  return class extends Component {
    componentDidUpdate(newProps){
      //TODO figure out playing and pausing
    }

    render(){
      return (
        <div className="slide">
          <video className="video" autoplay="true" controls="false">
            <source src={slide.content.videoSrc} type="video/mp4" />
          </video>
        </div>
      )
    }
  }
}