
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const port = 8001;  

let number = 1;

io.on('connection', function(socket){
  console.log("connected");

  const i = setInterval(()=>{
    console.log("emiting for "+number);
    socket.emit('slide:change',{
        background:{color:'black'},
        type:'text',
        text:["Here's some text","And Another Line","Number:"+number++]
    });
  },2000);

  socket.on('disconnect',()=>{
    console.log("disconnected");
    clearInterval(i)

  })
});

const server = http.listen(port, function(err) {  
  if (err) {
    console.log(err);
  } else {
    console.log('listening',port)
  }
});