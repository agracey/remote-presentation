'use strict';

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var port = 8001;

var number = 1;

io.on('connection', function (socket) {
  console.log("connected");

  var i = setInterval(function () {
    console.log("emiting for " + number);
    socket.emit('slide:change', {
      background: { color: 'black' },
      type: 'text',
      text: ["Here's some text", "And Another Line", "Number:" + number++]
    });
  }, 2000);

  socket.on('disconnect', function () {
    console.log("disconnected");
    clearInterval(i);
  });
});

var server = http.listen(port, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('listening', port);
  }
});