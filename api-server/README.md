This server contains the following modules
  - An API that allows realtime pushes to displays
  - A set of models for persisting presentations and playlists
  - An API for working with the data
  - An API for realtime "playback"
  - Hosting of Assets
  - A set of pipes to connect everything


The Socket.io server should do the following:
  - Listen for new connections and their subsequent ID to be sent
  - Consume a Pipe for next slide_ids
  - Consume a Pipe for alert text
  - Publish on a Pipe for display connection status


The Model is:
  - Playlist
    - playlist_id
    - [presentation_id]
    - name
    - description
  - Presentation
    - presentation_id
    - [slide_id] // to allow for repeats, we can parse for "REPEAT:<slide_id>"
    - name
    - description
  - Slide
    - slide_id
    - type 
    - background
    - [text]
    - asset_id //asset
  - Asset
    - asset_id
    - content_location



The data API should allow for:
  - editing of playlists
  - editing of presentations
  - editing of slides
  - uploading and deleting of assets
  - 

The playback API should allow for:
  - querying the current status
  - going to any slide
  - setting current playlist
  - setting current presentation
  - going to next slide
  
  All of these actions return the new "current" status of the system

