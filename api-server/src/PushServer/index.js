var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Stream = require('stream');
var Sequelize = require('sequelize');

const port = 8002;

export default class extends Stream.Duplex {
  constructor(Model) {
    super({objectMode: true});
    this.Model = Model;
    this.socket = null;
    this.currentPlaylistId=1;
    
    this.currentSlideId=2;
  }
  _write(obj,enc,cb) {
    cb();
    //obj should have property: "action" as well as any data that's needed for that action

    console.log("got:",obj.action,obj.id)
    switch(obj.action) {
      case "next":
          this.handleNext()
          break;
      case "prev":
          this.handlePrev()
          break;
      case "jump":
          this.handleJump(obj.id);
          break;
      case "blank":
          this.handleBlank();
          break;
      default: 
          console.log("ERROR: action pushed to PushServer was not known")
          return;
    }
  }

  _read(size) {
  }

  start() {
    io.on('connection', (socket) => {
      console.log('connection');
      this.socket = socket;
      this.push({action:'status',display_name:'TBD',state:'connected'});

      this.pushCurrentTo(socket);

      socket.on('disconnect', () => {
        console.log('disconnect');
        this.socket = null;
        this.push({action:'status',display_name:'TBD',state:'disconnected'});
      })
    });

    this.server = http.listen(port, function (err) {
      if (err) {console.log(err);}
    });
  }

  // push when connection is set up or re-established
  pushCurrentTo(socket) {
    this.Model.Slide.findById(this.currentSlideId).then((currentSlide)=>{
      socket.emit('slide:change', this.packageSlide(currentSlide))
      this.push({action:'state',state:{currentSlide:slide,nextSlide:{type:"blank"}}});
      
    })
  }

  emitSlide(slide) {
    io.emit('slide:change', slide);
    this.push({action:'state',state:{currentSlide:slide,nextSlide:{type:"blank"}}});
  }

  handleBlank() {
    this.emitSlide({
      type:"text",
      background:{color:"black"},
      contents:{text:[]}
    });
  }

  handleNext() {
    this.Model.Slide.findById(this.currentSlideId).then((currentSlide)=>{
      console.log("Current: ",currentSlide);
      currentSlide.getNextSlide().then((nextSlide)=>{
        if(!nextSlide) {
          console.log("moving to next prezo")
          this.handleNextPrezo();
          return;
        }
        console.log("Next: ",nextSlide);
        this.currentSlideId = nextSlide.id
        this.emitSlide(this.packageSlide(nextSlide));
      });
    })
  }
  handleNextPrezo() {
    this.Model.Slide.findById(this.currentSlideId).then((s)=>{
      s.getPresentation().then(({id})=>{
        return this.Model.PlaylistOrder.findOne({
          where: {
            playlistId: this.currentPlaylistId,
            presentationId: id
          }
        })
      }).then((playlistOrder) => {
        return this.Model.PlaylistOrder.findOne({
          attributes: {
            include: [
              [Sequelize.fn("MIN", Sequelize.col('order')), 'nextOrder']
            ]
          },
          where: {
            playlistId: playlistOrder.playlistId,
            order: { $gt: playlistOrder.order }
          }
        })
      }).then((nextPlaylistOrder) => {
        if (nextPlaylistOrder != null) {
          return this.Model.Presentation.findById(nextPlaylistOrder.presentationId);
        } else {
          this.handleBlank();
        }
      }).then((nextPresentation) => {
        if (nextPresentation != null) {
          return nextPresentation.getStartingSlide();
        }
      }).then((nextSlide) => {
        if (nextSlide != null) {
          this.emitSlide(this.packageSlide(nextSlide));
        }
      }).catch((err) => console.error("While handleNextPrezo", this.currentPlaylistId, s, err));
    });
  }

  handlePrev() {
    //previous slide will have it's nextSlide as this.currentSlideId
    this.Model.Slide.findOne({where:{nextSlideId:slide_id}}).then((nextSlide)=>{
      console.log("Backing up to: ",nextSlide);
      this.currentSlideId = nextSlide.id
      this.emitSlide(this.packageSlide(nextSlide));
    });
  }

  handleJump(slide_id) {
    this.Model.Slide.findById(slide_id).then((nextSlide)=>{
      console.log("Jumping To: ",nextSlide);
      this.currentSlideId = nextSlide.id
      this.emitSlide(this.packageSlide(nextSlide));
    });
  }

  packageSlide(model_slide) {
    return {
      type:model_slide.type,
      background:JSON.parse(model_slide.background),
      content:JSON.parse(model_slide.content)
    }
  }
}