import Models from './Models/index.js';
import PushServer from './PushServer/index.js';
import PlaybackAPI from './PlaybackAPI/index.js';
import PlaylistAPI from './PlaylistAPI/index.js';
import AssetServer from './AssetServer/index.js';
var Stream = require('stream');

/* 
 * NOTES:
 *  The reasonI structured the project this way 
 *  is so there's no issue moving to an HA architecture.
 *  I figured each API needed to do only one thing and 
 *  could be easliy ripped and replaced. They needed to have
 *  an easy way to talk to each other so I made them all event
 *  sources and sinks. They listen for events they care about
 *  
 *  Possibly a better way would be to have injection a 
 *  single event emmitter that would broker everything
 *  but I decided this model makes more sense if the different 
 *  modules are ever tied together using a real broken.
 * 
 *  Either way, it works and that's what matters
 *
 */


Models.loadDefault();

const pushServer = new PushServer(Models);
const playbackAPI = new PlaybackAPI(Models);
const playlistAPI = new PlaylistAPI(Models);
const assetServer = new AssetServer(Models);

// For instructions
playbackAPI.pipe(pushServer);
pushServer.pipe(playbackAPI);


//I just need to start each of these parts seperately on different ports. Eventually should be started as daemons in docker image
pushServer.start();
console.log("FINDME1")
playbackAPI.start();
console.log("FINDME2")
playlistAPI.start();
console.log("FINDME3")
assetServer.start();
console.log("FINDME4")