import Router from 'express';
export default function(Model){
  const routes = new Router();

  routes.put('/slides/:id',function(req,res){
    Model.Slide.findById(req.params.id).then(slide=> {
     
    })
  })

  routes.get('/presentation',function(req,res){
    Model.Presentation.findAll().then((preso_list)=>{
      const preso_map = preso_list.reduce((acc,curr)=>{
        curr.slides = [];
        acc[curr.id]=curr; 
        return acc
      },{});
      console.log(preso_map)
      //all slides since they are all used
      Model.Slide.findAll().then((slide_list)=>{
        slide_list.forEach((s)=>{
          console.log("slide:",s.presentationId)
          preso_map[s.presentationId].slides.push(s);
        })
        res.send(preso_map);

      })
      
    });
  });

  routes.get('/presentation/:id',(req,res)=>{
    Model.Presentation.findAll().then((p)=>{
      Model.Slide.findAll({where:{presentationId:p.id}}).then((slides)=>{
        p.slides = slides;
        res.send(p);

      })
    });
  });

  routes.post('/presentation',(req,res)=>{
    //Model.Slides.findAll().then(res);
  });

  routes.get('/playlist/',(req,res)=>{
    Model.Playlist.findAll().then((lis)=>{
      res.send(lis);
    }).catch(res.send);
  });

  routes.get('/playlist/:id',(req,res)=>{
    Model.Playlist.findAll().then((lis)=>{
      res.send(lis);
    }).catch(res.send);
  });

  //need to get body 
  routes.post('/playlist',(req,res)=>{
    //Model.Playlist.create(req.body).then(res.send).catch(res.send);
  });

  
  routes.put('/playlist/:play_id',(req,res)=>{
    //Model.Playlist.update({},{}).then(res.send).catch(res.send);
  });

  return routes;
}
