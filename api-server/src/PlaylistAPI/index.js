var app = require('express')();
var http = require('http').Server(app);
var Stream = require('stream');
import routes from './routes.js';

const port = 8004;

export default class PlaylistAPI {
  constructor(Model) {
    this.Model = Model;
    app.use('/',routes(Model));
  }

  start() {
    console.log("Starting Playlist API");
    this.server = http.listen(port, function (err) {
      if (err) {console.log(err);}
    });
  }
}