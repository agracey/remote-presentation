var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Stream = require('stream');

const port = 8003;

export default class extends Stream.Duplex {
  constructor(Model) {
    super({objectMode: true});
    this.Model = Model;
    this.socket = null;

    this.slideState;
    
  }
  _write(obj,enc,cb) {
    cb();
    console.log("reading:", obj);
    //this.sendCurrent();
    if(obj.action == 'status') {
      io.emit('event:connection', obj);
    }

    if(obj.action == 'state') {
      this.slideState = obj.state;
      this.emitCurrent();
    }
  }
  emitCurrent() {
    console.log("emitting current:",this.slideState);
    io.emit('state:current',this.slideState)
  }

  _read() {
  }

  start() {
    io.on('connection', (socket) => {
      console.log('playback connected');
      this.socket = socket;

      socket.on('disconnect', () => {
        console.log('playback disconnected');
      })

      socket.on('slide:next', () => {
        console.log('playback next');
        this.push({action:'next'})
      })

      socket.on('slide:prev', () => {
        console.log('playback prev');
        this.push({action:'prev'})
      })

      socket.on('slide:jump', (input) => {
        const {id} = JSON.parse(input);
        console.log('playback jump to',id);
        this.push({action:'jump',id})
      })

      socket.on('slide:blank', ({id}) => {
        this.push({action:'blank'})
      })

      socket.on('state:get',()=>{
        this.emitCurrent();
      })
      
    });

    this.server = http.listen(port, function (err) {
      if (err) {console.log(err);}
    });
  }
}