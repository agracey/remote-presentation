var Sequelize = require('sequelize');

class Model {
  constructor(database,username,password){
    this.sequelize = new Sequelize(database,username,password,{dialect:'sqlite', storage: 'database.sqlite'});

    this.Asset = this.sequelize.define("asset", {
      path: Sequelize.STRING
    });

    this.Slide = this.sequelize.define("slide", {
      name: Sequelize.STRING,
      type: Sequelize.STRING,
      background: Sequelize.STRING,
      content: Sequelize.STRING
    });
    
    this.Presentation = this.sequelize.define("presentation", {
      name: Sequelize.STRING,
      description: Sequelize.STRING
    });

    this.Playlist = this.sequelize.define("playlist", {
      name: Sequelize.STRING,
      description: Sequelize.STRING,
      slides:Sequelize.VIRTUAL
    });

    this.Slide.belongsTo(this.Asset,{as: 'asset'});
    this.Slide.belongsTo(this.Slide,{as: 'nextSlide', constraints: false, allowNull:true, defaultValue:null});
    this.Slide.belongsTo(this.Presentation, { as:'presentation'});
    this.Presentation.belongsTo(this.Slide,{ as: 'startingSlide',constraints: false, allowNull:true, defaultValue:null})

    this.PlaylistOrder = this.sequelize.define("PlaylistOrder", {
      order: Sequelize.INTEGER
      
    });

    //TODO many to many http://docs.sequelizejs.com/manual/tutorial/associations.html#belongs-to-many-associations
    this.Presentation.belongsToMany(this.Playlist, {through: 'PlaylistOrder'})
    this.Playlist.belongsToMany(this.Presentation, {through: 'PlaylistOrder'})
  }
  
  sync() {
    return this.sequelize.sync().then(()=>{return this;});
  }

  loadDefault() {
    this.sync(true).then(()=>{
      console.log("in load default and synced");

      Promise.all([
        
      this.Playlist.create({name:'Default',description:'Default Playlist'}),
      this.Playlist.create({name:'PL2',description:'Default Playlist2'}),
      this.Playlist.create({name:'PL3',description:'Default Playlist3'}),

      this.Presentation.create({name:'Song 1',description:''}),
      this.Presentation.create({name:'Song 2',description:''}),
      this.Presentation.create({name:'Song 3',description:''}),

      this.Slide.create({
        name:'Intro',
        type:'text',
        background:JSON.stringify({color:'black'}),
        content:JSON.stringify({text:['Intro Line 1','Intro Line 2']})
        }),

      this.Slide.create({
        name:'Ch',
        type:'text',
        background:JSON.stringify({color:'black'}),
        content:JSON.stringify({text:['Ch Line 1','Ch Line 2']})
        }),

      this.Slide.create({
        name:'V1',
        type:'text',
        background:JSON.stringify({color:'black'}),
        content:JSON.stringify({text:['V1 Line 1','V1 Line 2']})
        }),

      this.Slide.create({
        name:'V2',
        type:'text',
        background:JSON.stringify({color:'black'}),
        content:JSON.stringify({text:['V2 Line 1','V2 Line 2']})
        }),

      this.Asset.create({
        path:'testAsset.png',
        })
      ]).then((arr)=>{
        const pl1 = arr[0];
        const pl2 = arr[1];
        const pl3 = arr[2];
        const pr1 = arr[3];
        const pr2 = arr[4];
        const pr3 = arr[5];
        const i = arr[6];
        const ch = arr[7];
        const v1 = arr[8];
        const v2 = arr[9];
        const a1 = arr[10];

        return Promise.all([
          ch.setPresentation(pr1),
          v1.setPresentation(pr1),
          v2.setPresentation(pr1),
          i.setPresentation(pr2),
          pr1.setStartingSlide(ch),
          pr2.setStartingSlide(i),
          ch.setNextSlide(v1),
          v1.setNextSlide(v2),
          pl1.setPresentations([pr1],{through:{order:1}}),
          pl1.setPresentations([pr2],{through:{order:2}}),
          pl2.setPresentations([pr2], {through:{order:1}}),
          pl2.setPresentations([pr3], {through:{order:2}}),
          pl3.setPresentations([pr2], {through:{order:1}}),
          pl3.setPresentations([pr1], {through:{order:2}}),
          ch.setAsset(a1)
        ]);
        
      }).then((arr)=>{
        //console.log(arr);
      }).catch((err)=>{
        console.error(err);
      });
      
    })

  }

  parseBackup(contents) {
    throw "NOT IMPLEMENTED YET";
  }

  exportBackups() {
    throw "NOT IMPLEMENTED YET";
  }

}

export default new Model("database","username","password");