var app = require('express')();
var http = require('http').Server(app);
var multer = require('multer');
var upload = multer({dest:'assets/'});
var fs = require('fs');

const port = 8005;

export default class AssetServer {
  constructor(Model) {
    this.Model = Model;
    app.get('/asset/:assetId',this.hostAsset.bind(this));
    app.get('/asset/',this.listAsset.bind(this));
    app.post('/asset',upload.any(), this.createAsset.bind(this));
  }

  start() {
    this.server = http.listen(port, function (err) {
      if (err) {console.log(err);}
    });
  }

  //using root to start path from the POV of the player
  getAssetPathForSlideId(slideId,root) {
    return this.Model.Slide.findById({slideId}).then((slide)=>{
      return root+":"+port+"/assets/"+slide.assetId;
    });
  }

  listAsset(req,res) {
    this.Model.Asset.findAll().then((assets)=>{
      res.send(assets);
    }).catch((err)=>{
      res.send(err);
    });
  }
  hostAsset(req,res) {
    const assetId = req.params.assetId;
    this.Model.Asset.findById(assetId).then((asset)=>{
      res.sendFile(asset.path,{root:'./assets'});
    }).catch((err)=>{
      res.status(404).send(err);
    });
  }

  createAsset(req,res) {
    
    Promise.all(req.files.map((file)=>{
      return new Promise((resolve,reject)=>{
        fs.rename(file.path,'assets/'+file.originalname,(err,r)=>{
          if(!err) resolve();
          else reject();
        })
      }).then(()=>{
        return this.Model.Asset.create({path:file.originalname})
      })
    })).then((f)=>{
      res.sendStatus(204);
    }).catch((err)=>{
      res.send(err);
    })
  }
}